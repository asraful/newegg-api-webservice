.. NewEgg Product Review API documentation master file, created by
   sphinx-quickstart on Mon Nov 11 14:10:23 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NewEgg Product Review API's documentation!
=====================================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Getting Started
===============

The NewEgg Product Review API allows product manufacturers to query all reviews of their products sold on the website `www.newegg.com <http://www.newegg.com>`_. With the help of the site's public API which allows users to quickly pull JSON formatted review data, we have generated a MongoDB database which is hosted on Heroku, and provided an intuitive User Interface to make querying much more easily accessible.  

Running the API Locally
=======================

To run the service locally you must first have MongoDB installed, and then the scripts from our repository located at `https://bitbucket.org/sreisman/newegg-api-webservice/overview <https://bitbucket.org/sreisman/newegg-api-webservice/overview>`_, can be ran to populate your database.  An example pipeline would first pull data from the NewEgg website using the get_all_products_info.py file, and inserted into a local mongo instance using the mongo_import.py script.  With our directory structure this would look like the following::

	python get_all_products_info.py
	cd backend
	cat AllProducts.csv | python ../mongo_import.py products

Here, in the last line, "products" is the name you specify for the database.  If the database specified already exists, the code will not run so that you don't overwrite data.  You must specifiy a new database name to import new data.  