# ==================================================================================================== #
# Return json formatted reviews for a list of products(json)                                           #
# Usage:                                                                                               # 
#   1) python get_reviews.py '{"products":[{"product":"N82E16814150506"}, {"product":"40-119-068"}]}'  #
#   2) import this module in your program and then call:                                               # 
#               reviews.GET('{"products":[{"product":"N82E16814150506"}, {"product":"40-119-068"}]}')  #
# ==================================================================================================== # 


import sys
import json
import requests


#reviews = []
_newegg = "http://www.ows.newegg.com/Products.egg/"



#
# Read the input string as a json object
#
def read_json(input_str):
    try:
        data = json.loads(input_str)            
        return data['products']
    except Exception, ex:
        print "Invalid JSON...Bad request.."
        return None



#
# GET request
#
def GET_response(url):    
    try:        
        response = requests.api.get(url)
        jsondata = json.loads(response.content)
        return jsondata
    except Exception, ex:
	    print "Bad response from " + url
	    return None




#
# Fetch the default page when no reviews exists
#
def crawl_when_no_review_found(product_id, page):        
    response = GET_response( _newegg + product_id + "/Reviews/" + str(page))
    dict = {}        
    dict['product'] = product_id
    dict['Rating'] = response['Summary']['Rating']
    dict['TotalReviews'] = response['Summary']['TotalReviews']
    dict['Reviews'] = response['Reviews']
    
    return dict    




#
# Fetch all reviews from first to last pages
#
def crawl_all_pages(product_id, MIN, MAX):    
    dict = {}        
    list = []
    for page in range(MIN, MAX+1):
        response = GET_response( _newegg + product_id + "/Reviews/" + str(page))
        #response = GET_response( _newegg + product_id + "/Reviews/")
        
      
        if page==1:
            dict['product'] = product_id
            dict['Rating'] = response['Summary']['Rating']
            dict['TotalReviews'] = response['Summary']['TotalReviews']
        
        #list = list + response['Reviews']            
        if response['Reviews'] != None:
            list = list + response['Reviews']
        

    dict['Reviews'] = list
    
    return dict    




# 
# Return all reviews if no review count given
#
def get_reviews_all(product_id):    
    #global reviews
    #reviews = []

    MIN = 1
    MAX = MIN   #max num of pages to crawl

    response = GET_response( _newegg + product_id + "/Reviews/")

    if response == None:
        return None

    
    if response['Summary']['TotalReviews'] != "":
        MAX = (int(response['Summary']['TotalReviews']) / 10) + 1         
        dict = crawl_all_pages(product_id, MIN, MAX)
    else:
        MAX = 1                
        dict = crawl_all_pages(product_id, MIN, MAX)

    
    #reviews.append(dict)    

    return dict




#
# Return all reviews of a product, considering the existing number of reviews, already in DB
# latest_count is the current reviews count in DB. So only have fetch the new reviews
#
def get_reviews_rest(product_id, latest_count):    
    #global reviews
    #reviews = []
    MIN = 1
    MAX = MIN   #max num of pages to crawl

    response = GET_response( _newegg + product_id + "/Reviews/")

    if response == None:
        return None

    
    if response['Summary']['TotalReviews'] != "":
        MAX = ( ( int(response['Summary']['TotalReviews']) - int(latest_count) ) / 10 ) + 1
        dict = crawl_all_pages(product_id, MIN, MAX)
    else:
        MAX = 1        
        #dict = crawl_when_no_review_found(data['product']['id'], MIN)
        dict = crawl_all_pages(product_id, MIN, MAX)

    
    #reviews.append(dict)    
    return dict



#
# put all reviews to a file
#
def make_json_output():
    file = open('product_reviews.json', 'w')
    output = json.dumps(reviews, indent = 4, separators=(',', ': '), sort_keys=True)
    file.write(output)
    file.close()




#
# program entry point, given, a json formatted product list
# 
def GET(json_product):
    #    global reviews
    
    reviews = []

    products = read_json(json_product)
    
    if products is None:        
        return 

    for data in products:
        if 'review-count' in data:
            if 'product' in data:
                #reviews = get_reviews_rest(data['product'], data['review-count'])            
                dict = get_reviews_rest(data['product'], data['review-count'])            
                reviews.append(dict)
        else:
            if 'product' in data:                    
                #reviews = get_reviews_all(data['product'])
                dict = get_reviews_all(data['product'])
                reviews.append(dict)

    reviews = {"products":reviews}
    reviews = json.dumps(reviews, indent = 4, separators=(',', ': '), sort_keys=True)    
    return reviews





if __name__ == '__main__':
    input_str = sys.argv[1]
    print GET(input_str)
    #make_json_output(GET(input_str))
