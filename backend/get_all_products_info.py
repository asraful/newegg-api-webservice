import config
import json
import requests
import csv


productList = []
file = open(config.CSV_FILE, "wa")	


def get_response(url):
	try:		
		response = requests.api.get(url)
		jsondata = json.loads(response.content)
		return jsondata
	except Exception, ex:
		print "error: get_response(): " + str(ex)
		return None



def post_response(url, node_id, page_num):
	try:
		response = requests.api.post(url, json.dumps({'NodeId':node_id, 'PageNumber':page_num}))		
		jsondata = json.loads(response.content)
		return jsondata		
	except Exception, ex:
		print "error: post_response(): " + str(ex)
		return None



def get_product_lists(subCategory):
	page = 1
	itemList = []
	try:
		#productListResponse = post_response('http://www.ows.newegg.com/Search.egg/Advanced', subCategory['NodeId'], page)	
		productListResponse = post_response(config.SEARCH_URL, subCategory['NodeId'], page)		

		while((productListResponse is not None) and productListResponse['ProductListItems']):
			productListResponse = productListResponse['ProductListItems']
			
			if(productListResponse is not None):
				
				for product in productListResponse:#['ProductListItems']:
					item = []								
					item.append(str(product['ItemNumber'].encode('utf-8')))
					item.append(str(product['Title'].encode('utf-8')).replace('\n', ' ').replace('\r', ''))										

					itemList.append(item)
				
				print str(subCategory['NodeId']) + ': ' + str(subCategory['Description']) + ', ' + str(page) +': ' + str(len(itemList))

			page += 1
			#productListResponse = post_response('http://www.ows.newegg.com/Search.egg/Advanced', subCategory['NodeId'], page)
			productListResponse = post_response(config.SEARCH_URL, subCategory['NodeId'], page)
		
	except Exception, ex:
		print "error: get_product_lists(): " + str(ex)

	return itemList




def add_header_to_csv():
	try:		
		writer = csv.writer(file, quoting=csv.QUOTE_ALL)
		writer.writerow(["Store", "Category", "SubCategory", "ItemNumber", "Title"])
	except Exception, ex:
		print ex




def add_all_subcat_items_to_csv(pList, store, cat, subCat):
	header = [store, cat, subCat]
	try:		
		writer = csv.writer(file, quoting=csv.QUOTE_ALL)
		
		for eachProduct in pList:			
			writer.writerow(header + eachProduct)

	except Exception, ex:
		print ex





add_header_to_csv()

stores = get_response(config.MENU_URL)

for eachMenu in stores:

	categories = get_response(config.CATEGORY_URL + '%i' % eachMenu['StoreID'])
	
	
	for eachCat in categories:

		subCategories = get_response(config.NAVIGATION_URL + '%i/%i/%i' % (eachMenu['StoreID'], eachCat['CategoryID'], eachCat['NodeId']))

		
		for eachSubCat in subCategories:

			items = get_product_lists(eachSubCat)
			
			add_all_subcat_items_to_csv(items, eachMenu['Title'], eachCat['Description'], eachSubCat['Description'])
			
			#productList.append(items)
	
	break
