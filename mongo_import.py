import sys 
import os
import re
from pymongo import *

#input_handle = open(sys.argv[1], "rU")
mongo_db_name = "local"

client = MongoClient()
db = client[mongo_db_name]

if db.posts.count() > 0:
	print("Mongo database %s is not empty. Please create new db" %mongo_db_name)
	sys.exit(1)

i = 0
tempProduct = {}
regex = re.compile("(\".*?\")")
for line in sys.stdin:
	for match in re.findall(regex, line):
		if i <= 4:
			tempProduct[i] = match
			i+=1
	i = 0
	
	document = {
		'Store' : tempProduct[0],
		'Category' : tempProduct[1],
		'SubCategory' : tempProduct[2],
		'ItemNumber' : tempProduct[3],
		'Title' : tempProduct[4]
	}

	db.hi.insert(document)

	print("Wrote " + str(document))
print("Wrote %d documents" % db.posts.count())

